Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Source: https://github.com/dgobbi/vtk-dicom
Upstream-Name: DICOM for VTK
Upstream-Contact: David Gobbi <david.gobbi@gmail.com>

Files: *
Copyright: Copyright (c) 2012-2017 David Gobbi
License: BSD-3-clause

Files: debian/*
Copyright: 2014, Mathieu Malaterre <malat@debian.org>
           2015-2018, Gert Wollny <gewo@debian.org>
License: BSD-3-clause

Files: DicomCli/vtkConsoleOutputWindow.*
Copyright:  Ken Martin, Will Schroeder, Bill Lorensen
License: BSD-3-clause

Files: CMake/FindDCMTK.cmake
Copyright: 2000-2009 Kitware, Inc., Insight Software Consortium.
           2009-2010 Mathieu Malaterre <mathieu.malaterre@gmail.com>
           2010 Thomas Sondergaard <ts@medical-insight.com>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
  * Neither name of Ken Martin, Will Schroeder, or Bill Lorensen nor the names
    of any contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
